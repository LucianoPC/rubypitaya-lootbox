## Run ruby pitaya metagame project
run:
	@docker-compose run --service-ports --rm rubypitaya bundle exec rubypitaya run

## Build project
build:
	@docker-compose build

## Kill all containers
kill:
	@docker rm -f $$(docker-compose ps -aq)

## Run tests
test:
	@docker-compose run --service-ports --rm rubypitaya bundle exec rspec

## Run ruby irb console
console:
	@docker-compose run --service-ports --rm rubypitaya bundle exec ruby ./bin/console

## Run bash on container
bash:
	@docker-compose run --service-ports --rm rubypitaya bash

## Create database
db-create:
	@docker-compose run --service-ports --rm rubypitaya bundle exec rake db:create

## Run migrations to database
db-migrate:
	@docker-compose run --service-ports --rm rubypitaya bundle exec rake db:migrate

## Drop database
db-drop:
	@docker-compose run --service-ports --rm rubypitaya bundle exec rake db:drop

## Reset database
db-reset:
	@docker-compose run --service-ports --rm rubypitaya bundle exec rake db:reset

## Generate Gemfile.lock
generate-gemfilelock:
	@docker run --rm -v "$(PWD)":/usr/src/app -w /usr/src/app ruby:2.6.6 bundle install

## Build image to production environment
prod-build-image:
	@docker build . -f docker/prod/Dockerfile -t rubypitaya-prod:latest

.DEFAULT_GOAL := show-help

.PHONY: show-help
show-help:
	@echo "$$(tput bold)Commands:$$(tput sgr0)"
	@echo
	@sed -n -e "/^## / { \
		h; \
		s/.*//; \
		:doc" \
		-e "H; \
		n; \
		s/^## //; \
		t doc" \
		-e "s/:.*//; \
		G; \
		s/\\n## /---/; \
		s/\\n/ /g; \
		p; \
	}" ${MAKEFILE_LIST} \
	| LC_ALL='C' sort --ignore-case \
	| awk -F '---' \
		-v ncol=$$(tput cols) \
		-v indent=22 \
		-v col_on="$$(tput setaf 6)" \
		-v col_off="$$(tput sgr0)" \
	'{ \
		printf "%s%*s%s ", col_on, -indent, $$1, col_off; \
		n = split($$2, words, " "); \
		line_length = ncol - indent; \
		for (i = 1; i <= n; i++) { \
			line_length -= length(words[i]) + 1; \
			if (line_length <= 0) { \
				line_length = ncol - indent - length(words[i]) - 1; \
				printf "\n%*s ", -indent, " "; \
			} \
			printf "%s ", words[i]; \
		} \
		printf "\n"; \
	}'



