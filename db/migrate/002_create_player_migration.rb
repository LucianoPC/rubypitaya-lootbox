require 'active_record'

class CreatePlayerMigration < ActiveRecord::Migration[5.1]

  enable_extension 'pgcrypto'

  def change
    create_table :players, id: :uuid do |t|
      t.belongs_to :user, type: :uuid
      t.string :name, null: false
      t.timestamps null: false
    end
  end
end
