require 'active_record'

class CreateUserMigration < ActiveRecord::Migration[5.1]

  enable_extension 'pgcrypto'

  def change
    create_table :users, id: :uuid do |t|
      t.timestamps
    end
  end
end
