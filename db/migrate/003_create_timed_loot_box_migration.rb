require 'active_record'

class CreateTimedLootBoxMigration < ActiveRecord::Migration[5.1]

  enable_extension 'pgcrypto'

  def change
    create_table :timed_loot_boxes, id: :uuid do |t|
      t.belongs_to :user, type: :uuid

      t.string   :code,        null: false
      t.integer  :state,       null: false, default: 0
      t.boolean  :forced_open, null: false, default: false
      t.datetime :open_at,     null: false

      t.timestamps
    end
  end
end
