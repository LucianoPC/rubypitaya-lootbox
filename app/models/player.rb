require 'active_record'

class Player < ActiveRecord::Base

  belongs_to :user

  validates_presence_of :name, :user

  def to_hash
    {
      name: name,
      userId: user_id,
    }
  end
end
