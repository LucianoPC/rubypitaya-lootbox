require 'active_record'

class TimedLootBox < ActiveRecord::Base

  ## Fields
  # id:          string
  #
  # code:        string
  # state:       string
  # forced_open: boolean
  # open_at:     datetime
  #
  # user:        User
  # user_id:     string
  ##

  ERROR_STATE_CANT_BE_OPENED = 

  belongs_to :user

  validate :validate_state_opened
  validates_presence_of :user, :code

  enum state: { closed: 0, opened: 1 }, _prefix: :state

  def open
    self.state = TimedLootBox.states[:opened]
  end

  def force_open
    self.forced_open = true
    open
  end

  def open_at_unix
    open_at.utc.to_i.to_s
  end

  def to_hash
    {
      userId: user_id,
      code: code,
      state: state,
      open_at: open_at_unix,
    }
  end

  private

  def validate_state_opened
    return true if forced_open
    return true unless state_opened?

    is_open_time_gone = open_at.utc <= Time.now.utc
    return true if is_open_time_gone

    errors.add(:state, ERROR_STATE_CANT_BE_OPENED)
  end
end
