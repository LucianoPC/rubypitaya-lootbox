class StatusCodes
  # Success codes
  CODE_OK = 'RP-200'

  # Error codes
  CODE_UNKNOWN = 'RP-000'
  CODE_HANDLER_NOT_FOUNDED = 'RP-001'
  CODE_ACTION_NOT_FOUNDED = 'RP-002'
  CODE_NOT_AUTHENTICATED = 'RP-003'
  CODE_AUTHENTICATION_ERROR = 'RP-004'

  class Connector
    CODE_UNKNOWN = 'PIT-000'
  end
end