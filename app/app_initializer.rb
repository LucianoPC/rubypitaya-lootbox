class AppInitializer < RubyPitaya::InitializerBase

  # method:     run
  # parameter:  initializer_content
  # attributes:
  #  - bll
  #    - class: InstanceHolder
  #    - methods:
  #      - add_instance(key, instance)
  #        - add any instance to any key
  #      - [](key)
  #        - get instance by key
  #  - redis
  #    - link: https://github.com/redis/redis-rb/

  def run(initializer_content)
    bll = initializer_content.bll

    playerBll = PlayerBLL.new

    bll.add_instance(:player, playerBll)
  end
end
