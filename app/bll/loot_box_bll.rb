class LootBoxBLL

  def new_timed_box(user_id, code, open_at)
    state_closed = TimedLootBox.states[:closed]

    TimedLootBox.new(
      user_id: user_id, 
      code: code, 
      state: state_closed, 
      forced_open: false,
      open_at: open_at.utc
    )
  end

  def get_timed_box(user_id, box_id)
    TimedLootBox.where(id: box_id, user_id: user_id).first
  end

  def get_timed_boxes(user_id)
    TimedLootBox.where(user_id: user_id)
  end

  def get_timed_boxes_that_can_be_opened(user_id)
    TimedLootBox.where('user_id = ? and open_at <= ?', user_id, Time.now.utc)
  end
end