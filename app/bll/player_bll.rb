class PlayerBLL

  def create_new_player(config)
    name = config['initial_player']['name']

    player = Player.new(name: name, user: User.new)
    player.save
    player
  end
end