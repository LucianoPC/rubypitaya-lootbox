class HelloWorldHandler < RubyPitaya::HandlerBase

  ROUTE_MATCH_FOUNDED = 'rubypitaya.helloWorldHandler.matchFounded'

  non_authenticated_actions :sayHello, :getError, :getPlayerInfo

  def sayHello
    response = {
      code: StatusCodes::CODE_OK,
      message: 'Hello!'
    }
  end

  def getError
    response = {
      code: 'RP-001',
      message: 'Error'
    }
  end

  def getPlayerInfo
    response = {
      code: StatusCodes::CODE_OK,
      message: 'Success',
      data: {
        name: "Guest",
        userId: "1234",
      }
    }
  end

  def findMatch
    user_id = @session.uid

    # TODO: On core put message as field on payload

    payload = {
      data: {
        opponentName: "Opponent",
      }
    }

    @postman.push_to_user(user_id, ROUTE_MATCH_FOUNDED, payload)

    response = {
      code: StatusCodes::CODE_OK,
      message: 'Success',
    }
  end
end
