class AuthenticationHandler < RubyPitaya::HandlerBase

  non_authenticated_actions :authenticate

  def authenticate
    user_id = @params[:userId]

    player = Player.find_by_user_id(user_id)
    player = @bll[:player].create_new_player(@config) if player.nil?

    @session.uid = player.user_id

    bind_session_response = @postman.bind_session(@session)

    unless bind_session_response.dig(:error, :code).nil?
      return response = {
        code: StatusCodes::CODE_AUTHENTICATION_ERROR,
        msg: 'Error to authenticate',
      }
    end

    response = {
       code: StatusCodes::CODE_OK,
       data: {
         userId: @session.uid
       }
    }
  end
end
