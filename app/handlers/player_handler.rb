class PlayerHandler < RubyPitaya::HandlerBase

  # class:      HandlerBase
  # attributes:
  #  - @bll
  #    - class: InstanceHolder
  #    - methods:
  #      - [](key)
  #        - info: get bll by key
  #
  #  - @redis
  #    - link: https://github.com/redis/redis-rb/
  #
  #  - @config
  #    - info: Hash with config json files inside of 'app/config'
  #    - example:
  #               Given you have the following file "app/config/initial_player.json"
  #               And this json content is {'name': 'Guest'}
  #               And you can get the initial player name
  #               Then you can run the following code: @config['initial_player']['name']
  #
  #  - @params
  #    - info: Special hash with the request parameters
  #    - link: https://api.rubyonrails.org/classes/ActionController/Parameters.html
  #
  #  - @session
  #    - attributes:
  #      - id          :: session id
  #      - uid         :: user id
  #      - data        :: session data
  #      - metadata    :: session data
  #      - frontend_id :: connector server id
  #
  #  - @postman
  #    - info: Send messages to server and clients
  #    - methods:
  #      - bind_session(session)
  #        - info:
  #                Send a session to connector, you can use to set the userId
  #                of the session, for example you can set an userId on
  #                @session, like `@session.uid = '123'`, and then you can
  #                bind this session with `@postman.bind_session(@session)`

  non_authenticated_actions :authenticate

  def authenticate
    user_id = @params[:userId]

    player = Player.find_by_user_id(user_id)
    player = @bll[:player].create_new_player(@config) if player.nil?

    @session.uid = player.user_id

    bind_session_response = @postman.bind_session(@session)

    unless bind_session_response.dig(:error, :code).nil?
      return response = {
        code: StatusCodes::CODE_AUTHENTICATION_ERROR,
        msg: 'Error to authenticate',
      }
    end

    response = {
       code: StatusCodes::CODE_OK,
       data: player.to_hash,
    }
  end

  def getInfo
    user_id = @session.uid

    player = Player.find_by_user_id(user_id)

    response = {
       code: StatusCodes::CODE_OK,
       data: player.to_hash,
    }
  end
end
